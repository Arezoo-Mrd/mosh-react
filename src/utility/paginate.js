export  function paginate(items, pageNumber, pageSize) {
    const startIndex = (pageNumber - 1) * pageSize
    const result = items.slice(startIndex, pageSize * pageNumber )

    return result
}