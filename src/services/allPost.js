export async function getAllPost() {
    const url = 'https://jsonplaceholder.typicode.com/comments'
    try {
        const res = await fetch(url, {
            method: 'get',
            headers: {
                'Content-type': 'application/json'
            }
        })
        const finalResponse = await res.json();
        return finalResponse
    } catch (error) {
        alert(error)
    }
}