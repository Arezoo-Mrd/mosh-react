import React, { Component } from "react";
class Counter extends Component {

  componentDidUpdate(prevProps, prevState) {
    console.log('prevProps', prevProps)
    console.log('prevState', prevState)
    if(prevProps.counter.value !== this.props.counter.value){
// Ajax call and get new data from server
    }
      
  }
  componentWillUnmount() {
    console.log('counter componentWillUnmount rendered')
  }

  render() {
    console.log('Counter - Rendered')
    return (
      <div className="flex items-center justify-between w-2/6 mb-2">
        <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        <button onClick={() => this.props.onIncrement(this.props.counter)} className="bg-gray-600 hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-800">+</button>
        <button disabled = {this.props.counter.value === 0 ? 'disabled' : ''} onClick={() => this.props.onDecrement(this.props.counter)} className="bg-zinc-600 hover:bg-zinc-700 focus:bg-zinc-700 active:bg-gray-800 disabled:bg-gray-200 disabled:text-black">-</button>
        <button onClick={() => this.props.onDelete(this.props.counter.id)} className="bg-red-600 hover:bg-red-700 focus:bg-red-800 active:bg-red-900">X</button>
      </div>
    );
  }


  getBadgeClasses() {
    let classes = "m-2 inline-flex items-center min-w-[40px] justify-center px-2 py-1 rounded text-xs font-bold leading-none ";
    classes += this.props.counter.value === 0 ? "bg-yellow-400" : "bg-blue-700 text-white";
    return classes;
  }

  formatCount() {
    const  {value}  = this.props.counter;
    return value === 0 ? "Zero" : value;
  }
}

export default Counter;
