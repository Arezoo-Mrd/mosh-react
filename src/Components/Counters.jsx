import React, { Component } from "react";
import Counter from "./Counter";

class Counters extends Component {
  render() {
    console.log('Counters - Rendered')
    const {onReset, counters, onDelete, onIncrement, onDecrement} = this.props
    return (
      <div className="flex">
        <button
          onClick={onReset}
          className="bg-neutral-500 hover:bg-neutral-600 focus:bg-neutral-700 active:bg-neutral-800"
        >
          reset
        </button>
        <div className="w-full">
          {counters.map((counter) => (
            <Counter
              key={counter.id}
              onDelete={onDelete}
              onIncrement={onIncrement}
              onDecrement = {onDecrement}
              counter={counter}

            />
          ))}
        </div>
      </div>
    );
  }
}

export default Counters;
