import propTypes from "prop-types";
const Pagination = (props) => {
  const { itemsCount, pageSize, currentPage, onPageChange } = props;

  const pagesCount = itemsCount / pageSize;
  if (pagesCount <= 1) return null;
  const pages = Array.from({ length: Math.ceil(pagesCount) }, (i, j) => j + 1);

  return (
    <div className="flex items-center justify-center w-full py-3 bg-white border-t border-gray-200 sm:px-6">
      <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-center">
        <div>
          <nav
            className="relative z-0 inline-flex -space-x-px rounded-md shadow-sm h-[28px]"
            aria-label="Pagination"
          >
            {pages.map((page) => (
              <a
                key={page}
                onClick={() => onPageChange(page)}
                className={`relative z-10 inline-flex w-[28px] justify-center items-center px-4 py-2 text-sm font-medium text-black border border-gray-300 cursor-pointer hover:bg-gray-100 ${
                  page === currentPage ? "bg-indigo-100" : "bg-white"
                }`}
              >
                {page}
              </a>
            ))}
          </nav>
        </div>
      </div>
    </div>
  );
};

Pagination.propTypes = {
  itemsCount: propTypes.number.isRequired,
  pageSize: propTypes.number.isRequired,
  currentPage: propTypes.number.isRequired,
  onPageChange: propTypes.func.isRequired,
};
export default Pagination;
