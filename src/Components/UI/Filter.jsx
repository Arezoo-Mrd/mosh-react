import { Component } from "react";
class Filter extends Component {
  render() {
      const {filterItems, textProperty, valueProperty, selectedItem, onItemSelect} = this.props
    return (
      <div className="w-48 text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white">
        {filterItems.map((g) => {
            return(
                <a
           onClick={() => onItemSelect(g)}
            key={g[valueProperty]}
            aria-current="true"
            className={`block w-full px-4 py-2 text-white border-b border-gray-200 rounded-t-lg cursor-pointer  dark:border-gray-600 ${selectedItem === g ?  'bg-gray-600' : 'dark:bg-gray-800'}`}
          >
            {g[textProperty]}
          </a>
            )
        }
          
        )}
      </div>
    );
  }
}
Filter.defaultProps  = {
    textProperty: 'name',
    valueProperty: '_id'
}
export default Filter;
