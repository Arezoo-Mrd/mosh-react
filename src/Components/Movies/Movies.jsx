import React, { Component } from "react";
import Pagination from "../UI/Pagination";
import { getMovies } from "./../../services/fakeMovieService";
import { getGenres } from "./../../services/fakeGenreService";
import { paginate } from "../../utility/paginate";
import Filter from "../UI/Filter";
import MoviesTable from "../Common/MoviesTable";
import _ from "lodash";
import { NavLink } from "react-router-dom";
import { withRouter } from "../../withRouter";
import SearchBox from "../Common/SearchBox";

class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    selectedGenre: null,
    currentPage: 1,
    pageSize: 4,
    sortColumn: { path: "title", order: "asc" },
  };

  componentDidMount() {
    const genres = [{ _id: "", name: "All Genres" }, ...getGenres()];
    this.setState({ movies: getMovies(), genres });
  }

  handleDeleteMovie = (id) => {
    const movies = this.state.movies.filter((m) => m._id !== id);
    this.setState({ movies });
  };

  handleLike = (movie) => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movie };
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };
  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleGenreSelect = (genre) => {
    this.setState({ selectedGenre: genre, currentPage: 1 });
  };
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  getPageData = () => {
    const {
      pageSize,
      currentPage,
      movies: allMovies,
      selectedGenre,
      sortColumn,
    } = this.state;
    const filtered =
      selectedGenre && selectedGenre._id
        ? allMovies.filter((m) => m.genre._id === selectedGenre._id)
        : allMovies;

    const sorted = _.orderBy(filtered, [sortColumn.path], sortColumn.order);

    const movies = paginate(sorted, currentPage, pageSize);

    return {
      totalCount: filtered.length,
      data: movies,
    };
  };

  render() {
    const { length: movieCount } = this.state.movies;
    const { pageSize, currentPage, genres, selectedGenre, sortColumn } =
      this.state;

    if (movieCount === 0)
      return (
        <h6 className="py-6 text-center text-red-500">
          There are no movies in database
        </h6>
      );
    const { totalCount, data: movies } = this.getPageData();
    return (
      <div className="px-5">
        <div className="flex items-center justify-around">
          <Filter
            filterItems={genres}
            selectedItem={selectedGenre}
            onItemSelect={this.handleGenreSelect}
          />
          <div className="flex flex-col">
            <div className="flex justify-between">
              <div className="col-span-2 pt-2">
                <NavLink to={'/movies/new'} className="block py-2 pl-3 pr-4 text-white bg-gray-900 rounded md:p-4 dark:text-white">
                  New Movies
                </NavLink>
              </div>
              <h6 className="py-5">
                Showing {totalCount} movies in the database
              </h6>
            </div>
            <div className="py-3">
              <SearchBox />
            </div>
            <MoviesTable
              movies={movies}
              sortColumn={sortColumn}
              onLike={this.handleLike}
              onDelete={this.handleDeleteMovie}
              onSort={this.handleSort}
            />
          </div>
        </div>
        <Pagination
          itemsCount={totalCount}
          pageSize={pageSize}
          currentPage={currentPage}
          onPageChange={this.handlePageChange}
        />
      </div>
    );
  }
}

export default withRouter(Movies);
