import React from "react";
import Form from "./Common/Form";


class LoginForm extends Form {
  state = {
    data: {
      username: "",
      password: "",
    },
    errors: {},
  };



  doSubmit = () => {
    // call the server
    console.log("submitted");
  };



  render() {
    return (
      <div>
        <h1>Login</h1>
        <div className="mt-5">
          <form onSubmit={this.handleSubmit}>
            <div className="overflow-hidden rounded shadow">
              <div className="px-4 py-5 bg-white sm:p-7">
                <div className="grid grid-cols-12 gap-6">
                  {this.renderInput('username','Username')}
                  {this.renderInput('password','Password', 'password')}
                </div>
              </div>
              {this.renderButton('Login')}
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default LoginForm;
