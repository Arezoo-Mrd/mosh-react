import { Component } from "react";
import _ from "lodash";
import { withRouter } from "../../withRouter";

// columns: array
// onLike: function
//
class TableBody extends Component {
  renderCell = (item, column) => {
    if (column.content) return column.content(item);

    return _.get(item, column.path);
  };
  createKey = (item, column) => {
    return item._id + (column.path || column.key);
  };
  render() {
    const { data, columns } = this.props;
    return (
      <tbody>
        {data.map((item) => (
           
          <tr className="cursor-pointer" key={item._id} onClick = {() => this.props.navigate(`/movies/${item._id}`)}>
              {columns.map((column) => (
                <td key={this.createKey(item, column)}>
                  {this.renderCell(item, column)}
                </td>
              ))}
          </tr>
         
        ))}
      </tbody>
    );
  }
}

export default withRouter(TableBody);
