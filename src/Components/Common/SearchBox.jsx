const SearchBox = ({ value, onChange }) => {
  return (
    <input
      type="text"
      name="query"
      className="my-3 shadow-md outline-none w-[100%] p-3"
      placeholder="search..."
      value={value}
      onChange={(e) => onChange(e.currentTarget.value)}
    />
  );
};

export default SearchBox;
