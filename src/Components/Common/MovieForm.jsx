import Joi from "joi-browser";
import { getGenres } from "../../services/fakeGenreService";
import { getMovie, saveMovie } from "../../services/fakeMovieService";
import { withRouter } from "../../withRouter";
import Form from "./Form";

class MovieForm extends Form {
  state = {
    data: {
      title: "",
      genreId: "",
      numberInStock: "",
      dailyRentalRate: "",
    },
    genres: [],
    errors: {},
  };

  schema = {
    _id: Joi.string(),
    title: Joi.string().required().label("Title"),
    genreId: Joi.string().required().label("Genre"),
    numberInStock: Joi.number()
      .required()
      .min(0)
      .max(100)
      .label("Number in stock"),
    dailyRentalRate: Joi.number()
      .required()
      .min(0)
      .max(10)
      .label("Daily Rental rate"),
  };

  componentDidMount() {
    const genres = getGenres();
    this.setState({ genres });

    const { params } = this.props;
    const movieId = params.id || params;
    if (Object.keys(params).length === 0) return;

    const movie = getMovie(movieId);
    if (!movie) this.props.navigate("/not-found");

    this.setState({ data: this.mapToViewModal(movie) });
  }

  mapToViewModal(movie) {
    return {
      _id: movie._id,
      title: movie.title,
      genreId: movie.genre._id,
      numberInStock: movie.numberInStock,
      dailyRentalRate: movie.dailyRentalRate,
    };
  }
  doSubmit = () => {
    saveMovie(this.state.data);

    this.props.navigate("/movies");
  };
  render() {
    console.log("this.props: ", this.props);
    return (
      <div>
        <h1>Movie Form</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="overflow-hidden rounded shadow">
            <div className="px-4 py-5 bg-white sm:p-7">
              <div className="grid grid-cols-12 gap-6">
                {this.renderInput("title", "Title")}
                {this.renderSelect("genreId", "Genre", this.state.genres)}
                {this.renderInput("numberInStock", "Number in Stock")}
                {this.renderInput("dailyRentalRate", "Rate", "number")}
              </div>
            </div>
            {this.renderButton("Save")}
          </div>
        </form>
      </div>
    );
  }
}

export default withRouter(MovieForm);
