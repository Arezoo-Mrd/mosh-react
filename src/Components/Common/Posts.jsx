import React, { useEffect, useState } from "react";
import { getAllPost } from "../../services/allPost";
import { FixedSizeList as List } from "react-window";

const Posts = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const allPost = async () => {
      const res = await getAllPost();
      setData(res);
    };
    allPost();
  }, []);

  const Row = ({ index, style }) => (
    <div style={style}>
      body{index}: <span>{data[index].body}</span>
    </div>
  );

  return (
    <List height={400} itemCount={data.length} itemSize={35} width={"100%"}>
      {Row}
    </List>
  );
};

export default Posts;
