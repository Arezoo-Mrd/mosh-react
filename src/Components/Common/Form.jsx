import Input from "./Input";
import { Component } from "react";
import { validEmail } from "../../utility/regex";
import Joi from "joi-browser";
import Select from "./Select";

class Form extends Component {
  state = {
    data: {},
    errors: {},
  };

  
  validate = () => {
    const { data } = this.state;
    const options = { abortEarly: false };
    const { error } = Joi.validate(this.state.data, this.schema, options);

    if (!error) return null;

    const errors = {}
    for(let item of error.details) errors[item.path[0]] = item.message
    console.log('error: ',errors)
    return errors;
  };

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;

    this.doSubmit();
  };

  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data, errors });
  
  };

  renderButton(label) {
    return (
      <button
        disabled={this.validate()}
        className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
      >
        {label}
      </button>
    );
  }

  renderInput(name, label, type = "text") {
    const { data, errors } = this.state;
    return (
      <Input
        type={type}
        label={label}
        name={name}
        value={data[name]}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  }

  renderSelect (name, label, options) {
    const {data, errors} = this.state
    return (
     <Select 
     name={name}
     value = {data[name]}
     label = {label}
     options = {options}
     onChange = {this.handleChange}
     error = {errors[name]}
     />
    )

  }
}

export default Form;
