import React, { Component } from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import LoginForm from "./Components/Login";
import NavBar from "./Components/Navbar";
import Movies from "./Components/Movies/Movies";
import Counters from "./Components/Counters";
import "./App.css";
import RegisterForm from "./Components/RegisterForm";
import MovieForm from "./Components/Common/MovieForm";
import Posts from "./Components/Common/Posts";


class App extends Component {
  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 },
    ],
  };

  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  handleDecrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value--;
    this.setState({ counters });
  };

  handleDelete = (counterId) => {
    const counters = this.state.counters.filter((c) => c.id !== counterId);
    this.setState({ counters });
  };

  handleReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  render() {
    return (
      <React.Fragment>
        <NavBar
          totalCounters={this.state.counters.filter((c) => c.value > 0).length}
        />
        <main className="container">
          <Routes>
            <Route path="/movies" element={<Movies />} />
            <Route path="/movies/new" element={<MovieForm {...this.props}/>} />
            <Route path="/movies/:id" element={<MovieForm {...this.props} />} />
            <Route path="/login" element={<LoginForm />} />
            <Route path="/register" element={<RegisterForm />} />
            <Route path="/posts" element={<Posts />} />
            <Route
              path="/counters"
              element={
                <Counters
                  counters={this.state.counters}
                  onReset={this.handleReset}
                  onIncrement={this.handleIncrement}
                  onDecrement={this.handleDecrement}
                  onDelete={this.handleDelete}
                />
              }
            />
            <Route path="/not-found" element={<NotFound />} />
            {/* <Route path="*" element={<NotFound />} /> */}
          </Routes>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
